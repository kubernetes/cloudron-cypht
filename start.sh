#!/bin/bash

set -eu

if [[ ! -d /app/data/hm3 ]]; then
	mkdir -p /app/data/hm3/attachments
	mkdir -p /app/data/hm3/users
	mkdir -p /app/data/hm3/app_data
fi

if [[ ! -d /app/data/html ]]; then
	mkdir -p /app/data/html
fi

if [[ ! -f /app/data/hm3.ini ]]; then
	    cp /app/code/hm3.ini /app/data/hm3.ini
fi

chown -R cloudron:cloudron /app/data


#sed -i "s/^imap_auth_server=.*/imap_auth_server=${CLOUDRON_EMAIL_IMAP_SERVER}/" /app/data/hm3.ini
#sed -i "s/^imap_auth_tls=.*/imap_auth_tls=${CLOUDRON_EMAIL_IMAPS_PORT}/" /app/data/hm3.ini

echo "==> Starting App"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
