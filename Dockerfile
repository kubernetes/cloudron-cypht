FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=1.4.1

RUN mkdir -p /app/code
RUN mkdir -p /app/data
RUN mkdir -p /run/php/sessions

WORKDIR /app/code

RUN curl -L https://github.com/cypht-org/cypht/archive/refs/tags/v${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/cypht-${VERSION}/* /app/code/
RUN rm hm3.sample.ini
COPY hm3.sample.ini hm3.ini

RUN composer install
RUN php ./scripts/config_gen.php

RUN chown www-data:www-data /run/php/sessions
RUN rm -rf /var/lib/php/sessions && ln -s /run/php/sessions /var/lib/php/sessions

RUN rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf \
    && sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf \
    && a2disconf other-vhosts-access-log \
    && echo "Listen 8000" > /etc/apache2/ports.conf

COPY apache2.conf /etc/apache2/sites-available/app.conf
RUN a2ensite app

#RUN rm -rf /var/www/html
#RUN ln -s /app/code /var/www/html

COPY start.sh /app/code/
CMD [ "/app/code/start.sh" ]
